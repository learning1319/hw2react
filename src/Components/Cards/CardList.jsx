import { useState, useEffect } from "react";
import Card from "./Card";
import Modal from "../Modal/Modal";
import ModalHeader from "../Modal/ModalHeader";
import ModalClose from "../Modal/ModalClose";
import ModalFooter from "../Modal/ModalFooter";
import ModalWraper from "../Modal/ModalWraper";
import { useDispatch, useSelector } from "react-redux";
import { fetchCards } from "../../redux/cardList.slice/cards.slice";
import { modalOpen, modalClose } from "../../redux/modal.slice/modal.slice"
import { useContext } from "react";
import { ListContext } from "../Context/listContext";

export default function CardList(){
  const {listView} = useContext(ListContext);
    const [favoriteItems, setFavoriteItems] = useState([]);
    const [selectedProduct, setSelectedProduct] = useState(null);
    const [cartItems, setCartItems] = useState([]);
    const dispatch = useDispatch(); 
    
    const isModalOpen = useSelector(state=>state.modal.isModalOpen);
        
      /*список товарів */
      useEffect(() => {
       dispatch(fetchCards());   
      }, [dispatch]);

      const {cards} = useSelector(state => state.cards)
      
    const openModal = () => {
      dispatch(modalOpen());
  };

  // Function to close the modal
  const closeModal = () => {
      dispatch(modalClose());
  };
  
  /*оновлює інфо в змінній кошика */
  useEffect(() => {
    const dataFromStorage = localStorage.getItem('cartItems');
    const cartItemsFromStorage = dataFromStorage ? JSON.parse(dataFromStorage) : [];
      setCartItems(cartItemsFromStorage);
           
  }, []);
 

/*кнопка відкриття модалки для додавання в кошик */
  const addToCartHandler = () => {
    const updatedItems = cartItems.some((item) =>  item.article === selectedProduct.article) ? cartItems  :[...cartItems, selectedProduct];
    setCartItems(updatedItems);
    saveToStorage('cartItems', updatedItems);
    closeModal();
    handleCartClick()

  };
  
/* оновлює дані в стораджі*/
const productsInStorageUpdate = (keyStorage, callback, updateEvent) => {
  const itemsFromStorage = JSON.parse(localStorage.getItem(keyStorage)) || [];
  window.dispatchEvent(new Event(updateEvent));
  callback(itemsFromStorage);
};

   useEffect(() => {
      productsInStorageUpdate("favorites", setFavoriteItems);
      productsInStorageUpdate("cartItems", setCartItems);
    }, []);
  
  
  
    const handleCartClick = () => {
      productsInStorageUpdate("cartItems", setCartItems, "cartItemsUpdated");
    };
    const handleFavoriteClick = () => {
      productsInStorageUpdate("favorites", setFavoriteItems, "favoritesUpdated");
    };
  
/* товар по якому клінули*/

      const isSelected = (article) => {
        return favoriteItems.some((item) => item.article === article);
      };

      
/* додати до обраних*/
      const addToFavoritesHandler = (product) => {
        const updatedItems = favoriteItems.some((item) => item.article === product.article) 
          ? favoriteItems.filter((item) => item.article !== product.article)
          : [...favoriteItems, product];
      
        setFavoriteItems(updatedItems);
        saveToStorage('favorites', updatedItems);
        handleFavoriteClick()
      };
       
       /* оновити змінну з масивом обраного*/
       
        useEffect(() => {
          const favoriteItemsFromStorage = JSON.parse(localStorage.getItem('favorites')) || [];
          setFavoriteItems(favoriteItemsFromStorage);
        }, []);

       /* універсальна для збереження до сховища */

        const saveToStorage = (key, items) => {
            localStorage.setItem(key, JSON.stringify(items));
          };

/* змінює обраний продукт задає кнопці додати до кошика - тру*/
          const openAddToCart = (product) => {
            setSelectedProduct(product);             
              openModal();
              
          };

    return <div className= {` ${listView === "table"? "card-store_list-table" : "card-store_list"} `}>

       {cards && cards.length > 0 && 
         cards.map((product) => (
          <Card
          title={product.name}
          image={product.image}
          price={product.price}
          color={product.color}
          key={product.article}
          showAddButton={true}
          cardInCart={false}
          isSelected={isSelected(product.article)}
          onClickCart={()=>openAddToCart(product)}
          onClickFavorite={()=>addToFavoritesHandler(product)}
        />
      ))}
       { isModalOpen && selectedProduct && 
      <ModalWraper onClick={closeModal}>
        <Modal onClose={closeModal} >
        <ModalHeader > 
          <ModalClose onClick={closeModal} isCart={false} />
        </ModalHeader>
          <img src={selectedProduct.image} alt={selectedProduct.name} />
          <h2>{selectedProduct.name} </h2>
          <p>{selectedProduct.price} ₴</p>
          <p>{"Add selected product to cart"}</p>
        <ModalFooter firstText={"YES, add"} firstClick={addToCartHandler} secondaryText={"No, do not add"} secondaryClick={closeModal}/>
       </Modal>
      </ModalWraper>
       } 
        
    </div>
}