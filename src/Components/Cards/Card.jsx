import PropTypes from "prop-types";
import "./Cards.scss";
import ModalClose from "../Modal/ModalClose";
import { useContext } from "react";
import { ListContext } from "../Context/listContext";

function Card({
  image,
  title,
  color,
  article,
  price,
  isSelected,
  onClickCart,
  onClickFavorite,
  showAddButton,
  cardInCart,
  deliteFromCart,
}) {
  const {listView} = useContext(ListContext);
  return (
    <div className= {` ${listView === "table"? "card-store_table-item" : "card-store"} `}>
      {cardInCart ? (
        <ModalClose onClick={deliteFromCart} isCart={true}/>
      ) : (
        <div
          className={`card-store_select-star ${isSelected ? "selected-star" : ""}`}
          onClick={onClickFavorite}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
          >
            <path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z" />
          </svg>
        </div>
      )}
      <div>
      <img className="card-store_img" src={image} alt={title} />
      
      </div>
      
      <div className= {` ${listView === "table"? "card-store_text-block--table": "card-store_text-block"} `}>
        <div className="card-store_text-info">
          <h2 className="card-store_title">{title}</h2>
          <p className="card-store_product-color">{color}</p>
        </div>
        <div className= {` ${listView === "table"? "card-store_price-container--table": "card-store_price-container"} `}>
          <p className="card-store_price">{price} ₴</p>
          {showAddButton && (
            <button className="card-store_add-to-cart" onClick={onClickCart}>
              Add to cart
            </button>
          )}
        </div>
      </div>
    </div>
  );
}
Card.propTypes = {
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  article: PropTypes.string,
  price: PropTypes.number.isRequired,
  isSelected: PropTypes.bool,
  onClickCart: PropTypes.func,
  onClickFavorite: PropTypes.func,
};
export default Card;
