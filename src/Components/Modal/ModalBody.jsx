

export default function ModalBody({ children }) {
    return (
      <div className="modal_body">
        {children}
      </div>
    );
  }
  
