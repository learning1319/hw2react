import Button from "../Button/Button";

export default function ModalFooter({firstText, secondaryText, firstClick, secondaryClick}){
    
    
   
    return (<div className = "modal_footer">
    {firstText && <Button classNames="modal_button button-violet" onClick={firstClick} children={firstText}/>}
    {secondaryText && <Button classNames="modal_button button-white" onClick={secondaryClick} children={secondaryText}/>}
</div>

    
    )
}

