import { render, fireEvent } from '@testing-library/react';
import Modal from './Modal.jsx';
import ModalClose from './ModalClose.jsx';
import '@testing-library/jest-dom';


describe("<Header />", () => {
test('modal opens and closes correctly', () => {
  const handleClose = jest.fn();
  const { getByText, queryByText } = render(
    <Modal isOpen={true} onClose={handleClose}>
      <div>Modal content</div>
    </Modal>
  );

  expect(getByText('Modal content')).toBeInTheDocument();

  
  const { getByTestId } = render(<ModalClose onClick={handleClose}> &#10006;</ModalClose>);
   
  const closeButton = getByTestId('closeModal');
  fireEvent.click(closeButton);

  expect(handleClose).toHaveBeenCalledTimes(1);
  
})
test('modal is closed', () => {
    const { queryByText } = render(<Modal />);
    expect(queryByText('Modal content')).toBeNull();
  });
});
