import React from 'react';
import renderer from 'react-test-renderer';
import ModalClose from './ModalClose';

describe('ModalClose snapshots', () => {
  it('renders correctly without additional props', () => {
    const handleClick = jest.fn();
    const tree = renderer.create(<ModalClose onClick={handleClick} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders correctly with isCart prop set to true', () => {
    const handleClick = jest.fn();
    const tree = renderer.create(<ModalClose onClick={handleClick} isCart={true} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
