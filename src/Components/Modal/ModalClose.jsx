import React from 'react';

export default function ModalClose({ onClick, isCart }) {
  return (
    <button className ={`modal_close-button  ${isCart ? "cart_close-button" : null}`} data-testid='closeModal' onClick={onClick}>
      &#10006; 
    </button>
  );
}


