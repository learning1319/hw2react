import { useContext } from "react";
import { ListContext } from "../Context/listContext";
import React from 'react';

export default function ListToggler() {
    const {listView, setListView} = useContext(ListContext);
    function switchView(value){
        setListView(value)
    }
  return (
    <div className="btn-toggler_group" role="group" aria-label="ListToggler">
      <button type="button" className= {`btn btn-toggler_button ${listView === "table"? "active-button" : ""} `}  onClick={() => switchView('table')}>Table View</button>
      <button type="button" className={`btn btn-toggler_button ${listView === "card"? "active-button" : ""} `} onClick={() => switchView('card')}>Card View</button>
    </div>
  );
}

