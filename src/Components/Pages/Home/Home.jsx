
import CardList from "../../Cards/CardList";
import ListToggler from "../../ListToggler/ListToggler";
import { useContext } from "react";
import { ListContext } from "../../Context/listContext";

export default function Home(){

    const {listView} = useContext(ListContext);
    console.log(listView)

    return (
      <>
      <ListToggler />
      <CardList /> 
      
      </>
    );
    
    
    
}
