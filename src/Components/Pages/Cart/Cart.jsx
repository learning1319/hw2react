import Card from "../../Cards/Card";
import { useState, useEffect } from "react";
import Modal from "../../Modal/Modal";
import ModalHeader from "../../Modal/ModalHeader";
import ModalClose from "../../Modal/ModalClose";
import ModalFooter from "../../Modal/ModalFooter";
import ModalWraper from "../../Modal/ModalWraper";
import CartForm from "./CartForm";
import "./Cart.scss"


export function Cart() {
  const [cartItems, setCartItems] = useState([]);
  const [removeFromCart, setRemoveFromCart] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);

  const dataFromStorage = JSON.parse(localStorage.getItem("cartItems")) || [];

  function resetList (){
    saveToStorage("cartItems",[]);
    
    productsInStorageUpdate()
  }


  const productsInStorageUpdate = () => {    
      window.dispatchEvent(new Event("cartItemsUpdated"));
    setCartItems(dataFromStorage);
  };

  useEffect(() => {
    productsInStorageUpdate();
  }, []);
 

  const handleCartClick = () => {
    productsInStorageUpdate();
  };

  const isSelected = (article) => {
    return cartItems.some((item) => item.article === article);
  };

  const saveToStorage = (key, items) => {
    localStorage.setItem(key, JSON.stringify(items));
  };
  const closeModal = (e) => {
    setRemoveFromCart(false);
    setSelectedProduct(null);
  };
  const openModalCart = (product) => {
    setSelectedProduct(product);
    setRemoveFromCart(true);
  };
  const removeFomCartHandler = (selectedProduct) => {
    const updatedItems = cartItems.filter(
      (item) => item.article !== selectedProduct.article
    );

    setCartItems(updatedItems);
    saveToStorage("cartItems", updatedItems);
    handleCartClick();
    closeModal();
  };
 
  return (
    <div className="cart">
      <CartForm resetList={resetList}/>
    <div className="cart_card-list">
      {dataFromStorage.length > 0 ? (
        dataFromStorage.map((product) => (
          <Card
            title={product.name}
            image={product.image}
            price={product.price}
            color={product.color}
            key={product.article}
            cardInCart={true}
            isSelected={isSelected(product.article)}
            deliteFromCart={() => openModalCart(product)}
          />
        ))
      ) : (
        <h1 style={{ textWrap: "nowrap" }}>"Cart is empty"</h1>
      )}
      {removeFromCart && selectedProduct && (
        <ModalWraper onClick={closeModal}>
          <Modal onClose={closeModal}>
            <ModalHeader>
              <ModalClose onClick={closeModal} />
            </ModalHeader>
            <img src={selectedProduct.image} alt={selectedProduct.name} />
            <h2>{selectedProduct.name} </h2>
            <p>{selectedProduct.price} ₴</p>
            <p>{"Remove selected product from cart"}</p>
            <ModalFooter
              firstText={"YES, remove"}
              firstClick={() => removeFomCartHandler(selectedProduct)}
              secondaryText={"No, left"}
              secondaryClick={closeModal}
            />
          </Modal>
        </ModalWraper>
      )}
    </div>
    </div>
  );
}
