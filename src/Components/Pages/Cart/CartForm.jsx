
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";

export default function CartForm({resetList}) {
  const initialValues = {
    firstName: "",
    lastName: "",
    age: "",
    countryRegion: "",
    city: "",
    streetAddress: "",
    phone: "",
  };

  const validationSchema = Yup.object().shape({
    firstName: Yup.string().matches(/^[^\d]+$/, 'First name cannot contain numbers').required('First name is required'),
    lastName: Yup.string().matches(/^[^\d]+$/, 'Last name cannot contain numbers').required('Last name is required'),
    age: Yup.number().required('Age is required'),
    countryRegion: Yup.string().matches(/^[^\d]+$/, 'Country region cannot contain numbers').required('It\'s not a country region '),
    city: Yup.string().matches(/^[^\d]+$/, 'City cannot contain numbers').required('It\'s not a city'),
    streetAddress: Yup.string().required('It\'s not a Street Address'),
    phone: Yup.string().matches(/^\+(?:[0-9] ?){6,14}[0-9]$/, 'Invalid phone number').required('Phone number is required'),
});


  function onSubmit(values, {resetForm}) {
    const formData = {
        firstName: values.firstName,
        lastName: values.lastName,
        age: values.age,
        countryRegion: values.countryRegion,
        city: values.city,
        streetAddress: values.streetAddress,
        phone: values.phone,
      };

      const purchasedItems = JSON.parse(localStorage.getItem('cartItems'));   

      console.log('Purchased Items:', purchasedItems);
    console.log('Form Data:', formData);

    localStorage.removeItem('cartItems');

    resetForm();
    resetList();

  }

  return (
    <div className="cart_form-container">
        <h3>Check Out</h3>
    <p>Billing Details</p>
  <Formik 
  initialValues={initialValues} 
  onSubmit={onSubmit}
  validationSchema={validationSchema}
  >
    
    <Form>
        <div className="cart_form-item">
            <label htmlFor="firstName">First name*</label>
            <Field type="text" id="firstName" name="firstName" placeholder="First name"/>
          <ErrorMessage name="firstName" component="div" />
        </div>
        <div className="cart_form-item">
        <label htmlFor="lastName">Last name*</label>
            <Field type="text" id="lastName" name="lastName" placeholder="Last name"/>
          <ErrorMessage name="lastName" component="div" />
        </div>
        <div className="cart_form-item">
        <label htmlFor="age">Age*</label>
            <Field type="number" id="age" name="age" placeholder="Age"/>
          <ErrorMessage name="age" component="div" />
        </div>
        <div className="cart_form-item">
        <label htmlFor="countryRegion">Country region*</label>
            <Field type="text" id="countryRegion" name="countryRegion" placeholder="Country region"/>
          <ErrorMessage name="countryRegion" component="div" />
        </div>
        <div className="cart_form-item">
        <label htmlFor="city">City*</label>
            <Field type="text" id="city" name="city" placeholder="City"/>
          <ErrorMessage name="city" component="div" />
        </div>
        <div className="cart_form-item">
        <label htmlFor="streetAddress">Street address*</label>
            <Field type="text" id="streetAddress" name="streetAddress" placeholder="Street address"/>
          <ErrorMessage name="streetAddress" component="div" />
        </div>
        <div className="cart_form-item">
        <label htmlFor="phone">Phone*</label>
            <Field type="tel" id="phone" name="phone" placeholder="Phone"/>
          <ErrorMessage name="phone" component="div" />
        </div>
        <button className="button-violet" type="submit">Continue to delivery</button>
    </Form>

  </Formik>
  </div>)
  
}
