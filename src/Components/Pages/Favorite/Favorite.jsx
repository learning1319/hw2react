import Card from "../../Cards/Card";
import { useState, useEffect } from "react";
import Modal from "../../Modal/Modal";
import ModalHeader from "../../Modal/ModalHeader";
import ModalClose from "../../Modal/ModalClose";
import ModalFooter from "../../Modal/ModalFooter";
import ModalWraper from "../../Modal/ModalWraper";

export function Favorite() {
  const [favoriteItems, setFavoriteItems] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [addToCart, setAddToCart] = useState(false);
  const [cartItems, setCartItems] = useState(JSON.parse(localStorage.getItem("cartItems")) || []);

/* закрити модалку*/
  const closeModal = (e) => {
    setAddToCart(false);
    setSelectedProduct(null);
  };
/* універсальна для береження змін стораджа*/
  const saveToStorage = (key, items) => {
    localStorage.setItem(key, JSON.stringify(items));
    window.dispatchEvent(new Event("favoritesUpdated"));
  };
  /* оновити вміст стораджа*/
  const productsInStorageUpdate = (key, callback) => {
    const itemsFromStorage =
      JSON.parse(localStorage.getItem(key)) || [];
      callback(itemsFromStorage);
  };
 
/*чи є вибраний товар в улюбленому */
  const isSelected = (article) => {
    return favoriteItems.some((item) => item.article === article);
  };
/*відкрити модалку для додавання в кошик */
  const openAddToCart = (product) => {
    setSelectedProduct(product);
    setAddToCart(true);
  };

/* додавання в кошик*/

  const addToCartHandler = () => {
    const updatedItems = cartItems.some(
      (item) => item.article === selectedProduct.article
    )
      ? cartItems
      : [...cartItems, selectedProduct];
    setCartItems(updatedItems);
    saveToStorage("cartItems", updatedItems);
    closeModal();
    handleCartClick();
    window.dispatchEvent(new Event("cartItemsUpdated"));
  };
  /* видалення з улюбленого*/
  const removeFomFavoritesHandler = (product) => {
    const updatedItems = favoriteItems.filter(
      (item) => item.article !== product.article
    );
    setFavoriteItems(updatedItems);
    saveToStorage("favorites", updatedItems);
    handleFavoriteClick();
  };
 /* */
    useEffect(() => {
      productsInStorageUpdate("favorites", setFavoriteItems);
      productsInStorageUpdate("cartItems", setCartItems);
    }, []);
  
  /* */
  
    const handleCartClick = () => {
      productsInStorageUpdate("cartItems", setCartItems);
    };
    /* */
    const handleFavoriteClick = () => {
      productsInStorageUpdate("favorites", setFavoriteItems);
    };
   

  return (
    <div className="favorite card-list">
      {favoriteItems.length > 0 ? (
        favoriteItems.map((product) => (
          <Card
            title={product.name}
            image={product.image}
            price={product.price}
            color={product.color}
            key={product.article}
            showAddButton={true}
            isSelected={isSelected(product.article)}
            onClickCart={() => openAddToCart(product)}
            onClickFavorite={() => removeFomFavoritesHandler(product)}
          />
        ))
      ) : (
        <h1 style={{ textWrap: "nowrap" }}>"Favorite is empty"</h1>
      )}

      {addToCart && selectedProduct && (
        <ModalWraper onClick={closeModal}>
          <Modal onClose={closeModal}>
            <ModalHeader>
              <ModalClose onClick={closeModal} isCart={false} />
            </ModalHeader>
            <img src={selectedProduct.image} alt={selectedProduct.name} />
            <h2>{selectedProduct.name} </h2>
            <p>{selectedProduct.price} ₴</p>
            <p>{"Add selected product to cart"}</p>
            <ModalFooter
              firstText={"YES, add"}
              firstClick={addToCartHandler}
              secondaryText={"No, DELETE"}
              secondaryClick={closeModal}
            />
          </Modal>
        </ModalWraper>
      )}
    </div>
  );
}
