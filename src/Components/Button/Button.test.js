import { render, fireEvent } from '@testing-library/react';
import Button from './Button.jsx';
import React from 'react';
import renderer from 'react-test-renderer';


test('clicking button fires onClick handler', () => {
  const handleClick = jest.fn();
  const { getByTestId } = render(<Button onClick={handleClick}>YES, add</Button>);
  const button =  getByTestId('button');
  fireEvent.click(button);
  expect(handleClick).toHaveBeenCalledTimes(1);
});



describe('SimpleComponent snapshots', () => {
  it('renders correctly', () => {
    const handleClick = jest.fn();
    const tree = renderer.create(<Button onClick={handleClick}>YES, add</Button>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders correctly with disabled prop', () => {
    const handleClick = jest.fn();
    const tree = renderer.create(<Button onClick={handleClick} disabled>Disabled Button</Button>).toJSON();
    expect(tree).toMatchSnapshot();
  });
});

