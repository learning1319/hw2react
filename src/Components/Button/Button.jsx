export default function Button({type, classNames, onClick, children}){


    return (
        <button className={classNames}  data-testid='button' onClick={onClick} >{children.toUpperCase()}</button>
    )
}