
import "./App.css";
import { Cart, Favorite } from "./Components/Pages";
import Header from "./Components/Header/Header";
import Home from "./Components/Pages/Home/Home";
import PropTypes from "prop-types";
import {  Route, Routes } from "react-router-dom";
import { ListContext } from "../src/Components/Context/listContext";
import { useState, useEffect } from "react";
import "./Components/Modal/ModalStyle.scss";

function App() {
  const [listView, setListView] = useState('table');

  return (
    <>
    <ListContext.Provider value={{listView, setListView}}>
      <Header />
      <div>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/favorite" element={<Favorite />} />
          <Route path="/cart" element={<Cart />} />
        </Routes>
      </div>
      </ListContext.Provider>    
    </>
  );
}

App.propTypes = {
  selectedCounter: PropTypes.number,
  cartCounter: PropTypes.number,
};

export default App;
