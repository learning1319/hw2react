import { createSlice } from "@reduxjs/toolkit";


const initialState = {
    isModalOpen: false,
}

const modalSlice = createSlice({
    name: "ModalOpen",
    initialState,
    reducers: {
    modalOpen: (state, action)=>{        
      state.isModalOpen = true;
        },
     modalClose: (state, action)=>{            
         state.isModalOpen = false;
            }
    }

}

)

export const {modalOpen, modalClose} = modalSlice.actions
export default modalSlice.reducer;