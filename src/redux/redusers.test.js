import modalReducer, { modalOpen, modalClose } from './modal.slice/modal.slice';

describe('modal reducer', () => {
  it('should return the initial state', () => {
    expect(modalReducer(undefined, {})).toEqual({ isModalOpen: false });
  });

  it('should handle modalOpen', () => {
    const initialState = { isModalOpen: false };
    const action = modalOpen();
    const newState = modalReducer(initialState, action);
    expect(newState.isModalOpen).toEqual(true);
  });

  it('should handle modalClose', () => {
    const initialState = { isModalOpen: true };
    const action = modalClose();
    const newState = modalReducer(initialState, action);
    expect(newState.isModalOpen).toEqual(false);
  });
});
