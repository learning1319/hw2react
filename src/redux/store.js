import { configureStore } from "@reduxjs/toolkit";
// import { buildGetDefaultMiddleware } from "@reduxjs/toolkit/dist/getDefaultMiddleware";
import modalReduser from "./modal.slice/modal.slice";
import cardsReduser from "./cardList.slice/cards.slice";

export const store = configureStore({
reducer:{
modal: modalReduser,
cards: cardsReduser,

},
middleware: (getDefaultMiddleware)=>getDefaultMiddleware()

})