import { createAsyncThunk } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    cards: []
}
export const fetchCards = createAsyncThunk("cards/fetchCards", async()=>{
    try {
        const response = await fetch('../../../public/product.json'); 
        if (!response.ok) {
          throw new Error('Failed to fetch products');
        }
         const data = await response.json();
         return data;
         
      } catch (error) {
        console.error('Error fetching products:', error);
      }
})
const cardsSlice = createSlice({
    name: "cards",
    initialState,
    reducers: {},
    extraReducers: (builder) =>{
        builder.addCase(fetchCards.pending, (state)=>{
            state.status = "pending"})
        .addCase(fetchCards.rejected, (state)=>{
            state.status = "error"
           })
        .addCase(fetchCards.fulfilled, (state, action)=>{
            state.status = "done";
           
            state.cards = action.payload 
            })
    }

}

)

export default cardsSlice.reducer